import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class FileSystemDemo {
    private FileSystem fs;

    public FileSystemDemo(FileSystem fs) throws IOException, URISyntaxException {
        this.fs = fs;
    }


    public  void toonDirectoriesOpPad(Path p) throws IOException {
        FileStatus[] fileStatus = fs.listStatus(p);
        System.out.println("Print files voor " + p);
        for(FileStatus status : fileStatus){
            System.out.println(status.getPath().toString());
        }
    }



    public void maakEenDirectoryAan(Path path) throws IOException {
        fs.mkdirs(path);
    }
}
