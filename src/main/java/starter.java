;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class starter {
static FileSystem fs;


    public static void main(String[] args) throws IOException, URISyntaxException {
        // READ  FROM ARGUMENTS
        //TODO: Cleaner argument reading functionality
        String filePathString = args[0];
        //TODO: Do not pass credentials as an argument
        String awsKey= "";
        String awsSecret = "";
        if(args.length ==3){
            awsKey= args[1];
            awsSecret= args[2];
        }
        SparkConf conf = new SparkConf().setAppName("HadoopFileSystem").setMaster("local[4]");
/*        if(args.length ==3){
            awsKey= args[1];
            awsSecret= args[2];
        }*/


        SparkContext context = new SparkContext(conf);
        Configuration c = new Configuration();
/*        c.set("fs.s3a.access.key",awsKey);
        c.set("fs.s3a.secret.key",awsSecret);
        c.set("fs.s3a.aws.credentials.provider","org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider");
        conf.set("fs.s3a.access.key",awsKey);
        conf.set("fs.s3a.secret.key",awsSecret);
        conf.set("fs.s3a.aws.credentials.provider","org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider");*/

        FileSystem fs= FileSystem.get(new URI(filePathString),new Configuration());
        FileSystemDemo fsd = new FileSystemDemo(fs);
        System.out.println(conf.toDebugString());
        fsd.toonDirectoriesOpPad(new Path(filePathString + "/hadoopfilesystemtest/"));
        fsd.maakEenDirectoryAan(new Path(filePathString + "/hadoopfilesystemtest/aangemaakteMap" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))));
        fsd.toonDirectoriesOpPad(new Path(filePathString + "/hadoopfilesystemtest/"));

    }


}
